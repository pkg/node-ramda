"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.set = void 0;
const ArrayArbitrary_1 = require("./ArrayArbitrary");
function buildCompleteSetConstraints(constraints) {
    const minLength = constraints.minLength !== undefined ? constraints.minLength : 0;
    const maxLength = constraints.maxLength !== undefined ? constraints.maxLength : ArrayArbitrary_1.maxLengthFromMinLength(minLength);
    const compare = constraints.compare !== undefined ? constraints.compare : (a, b) => a === b;
    return { minLength, maxLength, compare };
}
function extractSetConstraints(args) {
    if (args[0] === undefined) {
        return {};
    }
    if (args[1] === undefined) {
        const sargs = args;
        if (typeof sargs[0] === 'number')
            return { maxLength: sargs[0] };
        if (typeof sargs[0] === 'function')
            return { compare: sargs[0] };
        return sargs[0];
    }
    if (args[2] === undefined) {
        const sargs = args;
        if (typeof sargs[1] === 'number')
            return { minLength: sargs[0], maxLength: sargs[1] };
        return { maxLength: sargs[0], compare: sargs[1] };
    }
    const sargs = args;
    return { minLength: sargs[0], maxLength: sargs[1], compare: sargs[2] };
}
function set(arb, ...args) {
    const constraints = buildCompleteSetConstraints(extractSetConstraints(args));
    const minLength = constraints.minLength;
    const maxLength = constraints.maxLength;
    const compare = constraints.compare;
    const arrayArb = new ArrayArbitrary_1.ArrayArbitrary(arb, minLength, maxLength, compare);
    if (minLength === 0)
        return arrayArb;
    return arrayArb.filter((tab) => tab.length >= minLength);
}
exports.set = set;
