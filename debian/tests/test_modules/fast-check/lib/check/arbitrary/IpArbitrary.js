"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ipV6 = exports.ipV4Extended = exports.ipV4 = void 0;
const ArrayArbitrary_1 = require("./ArrayArbitrary");
const ConstantArbitrary_1 = require("./ConstantArbitrary");
const IntegerArbitrary_1 = require("./IntegerArbitrary");
const OneOfArbitrary_1 = require("./OneOfArbitrary");
const StringArbitrary_1 = require("./StringArbitrary");
const TupleArbitrary_1 = require("./TupleArbitrary");
function ipV4() {
    return TupleArbitrary_1.tuple(IntegerArbitrary_1.nat(255), IntegerArbitrary_1.nat(255), IntegerArbitrary_1.nat(255), IntegerArbitrary_1.nat(255)).map(([a, b, c, d]) => `${a}.${b}.${c}.${d}`);
}
exports.ipV4 = ipV4;
function ipV4Extended() {
    const natRepr = (maxValue) => TupleArbitrary_1.tuple(ConstantArbitrary_1.constantFrom('dec', 'oct', 'hex'), IntegerArbitrary_1.nat(maxValue)).map(([style, v]) => {
        switch (style) {
            case 'oct':
                return `0${Number(v).toString(8)}`;
            case 'hex':
                return `0x${Number(v).toString(16)}`;
            case 'dec':
            default:
                return `${v}`;
        }
    });
    return OneOfArbitrary_1.oneof(TupleArbitrary_1.tuple(natRepr(255), natRepr(255), natRepr(255), natRepr(255)).map(([a, b, c, d]) => `${a}.${b}.${c}.${d}`), TupleArbitrary_1.tuple(natRepr(255), natRepr(255), natRepr(65535)).map(([a, b, c]) => `${a}.${b}.${c}`), TupleArbitrary_1.tuple(natRepr(255), natRepr(16777215)).map(([a, b]) => `${a}.${b}`), natRepr(4294967295));
}
exports.ipV4Extended = ipV4Extended;
function ipV6() {
    const h16Arb = StringArbitrary_1.hexaString({ minLength: 1, maxLength: 4 });
    const ls32Arb = OneOfArbitrary_1.oneof(TupleArbitrary_1.tuple(h16Arb, h16Arb).map(([a, b]) => `${a}:${b}`), ipV4());
    return OneOfArbitrary_1.oneof(TupleArbitrary_1.tuple(ArrayArbitrary_1.array(h16Arb, { minLength: 6, maxLength: 6 }), ls32Arb).map(([eh, l]) => `${eh.join(':')}:${l}`), TupleArbitrary_1.tuple(ArrayArbitrary_1.array(h16Arb, { minLength: 5, maxLength: 5 }), ls32Arb).map(([eh, l]) => `::${eh.join(':')}:${l}`), TupleArbitrary_1.tuple(ArrayArbitrary_1.array(h16Arb, { minLength: 0, maxLength: 1 }), ArrayArbitrary_1.array(h16Arb, { minLength: 4, maxLength: 4 }), ls32Arb).map(([bh, eh, l]) => `${bh.join(':')}::${eh.join(':')}:${l}`), TupleArbitrary_1.tuple(ArrayArbitrary_1.array(h16Arb, { minLength: 0, maxLength: 2 }), ArrayArbitrary_1.array(h16Arb, { minLength: 3, maxLength: 3 }), ls32Arb).map(([bh, eh, l]) => `${bh.join(':')}::${eh.join(':')}:${l}`), TupleArbitrary_1.tuple(ArrayArbitrary_1.array(h16Arb, { minLength: 0, maxLength: 3 }), ArrayArbitrary_1.array(h16Arb, { minLength: 2, maxLength: 2 }), ls32Arb).map(([bh, eh, l]) => `${bh.join(':')}::${eh.join(':')}:${l}`), TupleArbitrary_1.tuple(ArrayArbitrary_1.array(h16Arb, { minLength: 0, maxLength: 4 }), h16Arb, ls32Arb).map(([bh, eh, l]) => `${bh.join(':')}::${eh}:${l}`), TupleArbitrary_1.tuple(ArrayArbitrary_1.array(h16Arb, { minLength: 0, maxLength: 5 }), ls32Arb).map(([bh, l]) => `${bh.join(':')}::${l}`), TupleArbitrary_1.tuple(ArrayArbitrary_1.array(h16Arb, { minLength: 0, maxLength: 6 }), h16Arb).map(([bh, eh]) => `${bh.join(':')}::${eh}`), TupleArbitrary_1.tuple(ArrayArbitrary_1.array(h16Arb, { minLength: 0, maxLength: 7 })).map(([bh]) => `${bh.join(':')}::`));
}
exports.ipV6 = ipV6;
