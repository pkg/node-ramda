"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dictionary = exports.toObject = void 0;
const SetArbitrary_1 = require("./SetArbitrary");
const TupleArbitrary_1 = require("./TupleArbitrary");
function toObject(items) {
    const obj = {};
    for (const keyValue of items) {
        obj[keyValue[0]] = keyValue[1];
    }
    return obj;
}
exports.toObject = toObject;
function dictionary(keyArb, valueArb) {
    return SetArbitrary_1.set(TupleArbitrary_1.tuple(keyArb, valueArb), { compare: (t1, t2) => t1[0] === t2[0] }).map(toObject);
}
exports.dictionary = dictionary;
