"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.tuple = void 0;
const TupleArbitrary_generic_1 = require("./TupleArbitrary.generic");
function tuple(...arbs) {
    return new TupleArbitrary_generic_1.GenericTupleArbitrary(arbs);
}
exports.tuple = tuple;
