"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.genericTuple = exports.tuple = void 0;
const TupleArbitrary_generated_1 = require("./TupleArbitrary.generated");
Object.defineProperty(exports, "tuple", { enumerable: true, get: function () { return TupleArbitrary_generated_1.tuple; } });
const TupleArbitrary_generic_1 = require("./TupleArbitrary.generic");
Object.defineProperty(exports, "genericTuple", { enumerable: true, get: function () { return TupleArbitrary_generic_1.genericTuple; } });
