"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.context = void 0;
const symbols_1 = require("../symbols");
const ConstantArbitrary_1 = require("./ConstantArbitrary");
class ContextImplem {
    constructor() {
        this.receivedLogs = [];
    }
    log(data) {
        this.receivedLogs.push(data);
    }
    size() {
        return this.receivedLogs.length;
    }
    toString() {
        return JSON.stringify({ logs: this.receivedLogs });
    }
    [symbols_1.cloneMethod]() {
        return new ContextImplem();
    }
}
function context() {
    return ConstantArbitrary_1.clonedConstant(new ContextImplem());
}
exports.context = context;
