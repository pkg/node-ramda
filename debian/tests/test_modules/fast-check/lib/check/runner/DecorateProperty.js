"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.decorateProperty = void 0;
const SkipAfterProperty_1 = require("../property/SkipAfterProperty");
const TimeoutProperty_1 = require("../property/TimeoutProperty");
const UnbiasedProperty_1 = require("../property/UnbiasedProperty");
function decorateProperty(rawProperty, qParams) {
    let prop = rawProperty;
    if (rawProperty.isAsync() && qParams.timeout != null)
        prop = new TimeoutProperty_1.TimeoutProperty(prop, qParams.timeout);
    if (qParams.unbiased === true)
        prop = new UnbiasedProperty_1.UnbiasedProperty(prop);
    if (qParams.skipAllAfterTimeLimit != null)
        prop = new SkipAfterProperty_1.SkipAfterProperty(prop, Date.now, qParams.skipAllAfterTimeLimit, false);
    if (qParams.interruptAfterTimeLimit != null)
        prop = new SkipAfterProperty_1.SkipAfterProperty(prop, Date.now, qParams.interruptAfterTimeLimit, true);
    return prop;
}
exports.decorateProperty = decorateProperty;
