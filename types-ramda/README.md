# Installation
> `npm install --save @types/ramda`

# Summary
This package contains type definitions for ramda (https://ramdajs.com).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/ramda.

### Additional Details
 * Last updated: Thu, 20 Oct 2022 11:03:02 GMT
 * Dependencies: [@types/ts-toolbelt](https://npmjs.com/package/@types/ts-toolbelt)
 * Global values: `R`

# Credits
These definitions were written by [Scott O'Malley](https://github.com/TheHandsomeCoder), [Erwin Poeze](https://github.com/donnut), [Matt DeKrey](https://github.com/mdekrey), [Stephen King](https://github.com/sbking), [Alejandro Fernandez Haro](https://github.com/afharo), [Vítor Castro](https://github.com/teves-castro), [Simon Højberg](https://github.com/hojberg), [Samson Keung](https://github.com/samsonkeung), [Angelo Ocana](https://github.com/angeloocana), [Rayner Pupo](https://github.com/raynerd), [Nikita Moshensky](https://github.com/moshensky), [Ethan Resnick](https://github.com/ethanresnick), [Tomas Szabo](https://github.com/deftomat), [Maciek Blim](https://github.com/blimusiek), [Marcin Biernat](https://github.com/biern), [Rayhaneh Banyassady](https://github.com/rayhaneh), [Ryan McCuaig](https://github.com/rgm), [Drew Wyatt](https://github.com/drewwyatt), [John Ottenlips](https://github.com/jottenlips), [Nitesh Phadatare](https://github.com/minitesh), [Krantisinh Deshmukh](https://github.com/krantisinh), [Aram Kharazyan](https://github.com/nemo108), [Jituan Lin](https://github.com/jituanlin), [Philippe Mills](https://github.com/Philippe-mills), [Saul Mirone](https://github.com/Saul-Mirone), [Nicholai Nissen](https://github.com/Nicholaiii), [Jorge Santana](https://github.com/LORDBABUINO), [Mikael Couzic](https://github.com/couzic), [Nikita Balikhin](https://github.com/NEWESTERS), [Wang Zengdi](https://github.com/adispring), and [Marcus Blättermann](https://github.com/essenmitsosse).
